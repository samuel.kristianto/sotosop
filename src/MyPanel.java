
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author samuel kristianto
 */
public class MyPanel extends JPanel {
    
    private BufferedImage image, imageOutput;
    private String path;
    private int[][] temp;
    
    public MyPanel(String path) {
        this.path = path;
        reset();
    }
    public void reset(){
        try {
            image = ImageIO.read(new File(path));
            imageOutput = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        } catch (IOException ioe) {
            //ioe.printStackTrace();
        }
        revalidate();
        repaint();
    }
    public void greyScale() {

        int w = image.getWidth();
        int h = image.getHeight();

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                int pixel = image.getRGB(i, j);
                int red = (pixel >> 16) & 0xff;
                int green = (pixel >> 8) & 0xff;
                int blue = (pixel) & 0xff;
                
                int gray = (int)(0.21*red + 0.72*green + 0.07*blue);                
                Color color = new Color(gray,gray,gray);

                int rgb = (int)color.getRGB();
                image.setRGB(i, j, rgb);
            }
        }

        revalidate();
        repaint();
    }
    
    public void negative(){
        int w = image.getWidth();
        int h = image.getHeight();

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                int pixel = image.getRGB(i, j);
                int red = (pixel >> 16) & 0xff;
                int green = (pixel >> 8) & 0xff;
                int blue = (pixel) & 0xff;

                Color color = new Color(255-red,255-green,255-blue);
                int rgb = (int)color.getRGB();
                image.setRGB(i, j, rgb);
            }
        }

        revalidate();
        repaint();
    }
    
    public void rotate() {

        int w = image.getWidth();
        int h = image.getHeight();
        
        BufferedImage tempImage = new BufferedImage(h, w, BufferedImage.TYPE_INT_RGB);
        
        for(int sx = 0, y = 0; sx < w; sx++, y++){
            for(int sy = h-1 , x = 0; sy >= 0; sy--, x++){
                tempImage.setRGB(x, y, image.getRGB(sx, sy));
            }
        }
        image = tempImage;
        
        revalidate();
        repaint();
    }
    
    public void zoomIn(){
        int w = image.getWidth();
        int h = image.getHeight();
        
        int m = 0, n = 0;
        BufferedImage tempImage = new BufferedImage(w*2, h*2, BufferedImage.TYPE_INT_RGB);
        
        for(int i = 0 ; i < w ; i++){
            for(int j = 0 ; j < h ; j++){
                tempImage.setRGB(m, n, image.getRGB(i, j));
                tempImage.setRGB(m, n+1, image.getRGB(i, j));
                tempImage.setRGB(m+1, n, image.getRGB(i, j));
                tempImage.setRGB(m+1, n+1, image.getRGB(i, j));
                n+=2;
            }
            m+=2;
            n=0;
        }
        image = tempImage;
        imageOutput = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        revalidate();
        repaint();
    }
    
    public void zoomOut(){
        int w = image.getWidth();
        int h = image.getHeight();
        
        int m = 0, n = 0;
        BufferedImage tempImage = new BufferedImage(w/2, h/2, BufferedImage.TYPE_INT_RGB);
        
        for(int i = 0 ; i < w ; i+=2){
            for(int j = 0 ; j < h ; j+=2){
                tempImage.setRGB(m, n, image.getRGB(i, j));
                n++;
            }
            m++;
            n=0;
        }
        image = tempImage;
        imageOutput = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        revalidate();
        repaint();
    }
    
    public void flipVertical(){
        int w = image.getWidth();
        int h = image.getHeight();
        
        int k = h-1;
        BufferedImage tempImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        
        for(int i = 0 ; i < h ; i++){
            for(int j = 0 ; j < w ; j++){
                tempImage.setRGB(j, k, image.getRGB(j, i));
            }
            k--;
        }
       
        image = tempImage;
       
        revalidate();
        repaint();
    }
    
    public void brightness(){
        
        //valueBrightness -255 sampai 255
        int valueBrightness = customDialog("Brightness");
        
        if(valueBrightness!=Integer.MAX_VALUE){
            int w = image.getWidth();
            int h = image.getHeight();

            for(int i = 0 ; i < w ; i++){
                for(int j = 0 ; j < h ; j++){
                    int pixel = image.getRGB(i, j);

                    int red = (pixel >> 16) & 0xff;
                    int green = (pixel >> 8) & 0xff;
                    int blue = (pixel) & 0xff;

                    red = red + valueBrightness > 255 ? 255 : red + valueBrightness < 0 ? 0 : red + valueBrightness;
                    green = green + valueBrightness > 255 ? 255 : green + valueBrightness < 0 ? 0 : green + valueBrightness;
                    blue = blue + valueBrightness > 255 ? 255 : blue + valueBrightness < 0 ? 0 : blue + valueBrightness;

                    Color color = new Color(red,green,blue);
                    int rgb = (int)color.getRGB();
                    image.setRGB(i, j, rgb);
                }
            }


            revalidate();
            repaint();
        }
    }
    
    public void red(){
        
        int w = image.getWidth();
        int h = image.getHeight();
       
        for(int i = 0 ; i < w ; i++){
            for(int j = 0 ; j < h ; j++){
                int pixel = image.getRGB(i, j);
                int red = (pixel >> 16) & 0xff;
                
                Color color = new Color(red,0,0);
                int rgb = (int)color.getRGB();
                image.setRGB(i, j, rgb);
            }
        }
       
       
        revalidate();
        repaint();
    }
    
    public void green(){
        
        int w = image.getWidth();
        int h = image.getHeight();
       
        for(int i = 0 ; i < w ; i++){
            for(int j = 0 ; j < h ; j++){
                int pixel = image.getRGB(i, j);
                int green = (pixel >> 8) & 0xff;
                
                Color color = new Color(0,green,0);
                int rgb = (int)color.getRGB();
                image.setRGB(i, j, rgb);
            }
        }
       
       
        revalidate();
        repaint();
    }
    
    public void blue(){
        
        int w = image.getWidth();
        int h = image.getHeight();
       
        for(int i = 0 ; i < w ; i++){
            for(int j = 0 ; j < h ; j++){
                int pixel = image.getRGB(i, j);
                int blue = (pixel) & 0xff;
               
                Color color = new Color(0,0,blue);
                int rgb = (int)color.getRGB();
                image.setRGB(i, j, rgb);
            }
        }
       
       
        revalidate();
        repaint();

        
    }
    
    public void sepia(){
        int w = image.getWidth();
        int h = image.getHeight();
        
        for(int i = 0 ; i < w ; i++){
            for(int j = 0 ; j < h ; j++){
                int pixel = image.getRGB(i, j);
                
                int red = (pixel >> 16) & 0xff;
                int green = (pixel >> 8) & 0xff;
                int blue = (pixel) & 0xff;
                
                int tempRed = (int)(0.393*red + 0.769*green + 0.189*blue);
                int tempGreen = (int)(0.349*red + 0.686*green + 0.168*blue);
                int tempBlue = (int)(0.272*red + 0.534*green + 0.131*blue);
                
                red = tempRed > 255 ? 255:tempRed;
                green = tempGreen > 255 ? 255:tempGreen;
                blue = tempBlue > 255 ? 255:tempBlue;
                
                Color color = new Color(red,green,blue);
                int rgb = (int)color.getRGB();
                image.setRGB(i, j, rgb);
            }
        }
       
       
        revalidate();
        repaint();
    }
    
    public int customDialog(String title){

        JTextField value = new JTextField();
        
        JComponent[] inputs = new JComponent[] {
                new JLabel("Value : "),
                value
        };
        
        int result = JOptionPane.showConfirmDialog(null, inputs, title, JOptionPane.PLAIN_MESSAGE);
        
        if (result == JOptionPane.OK_OPTION && value.getText()!= null) {
            return Integer.valueOf(value.getText());
        } else {
            return Integer.valueOf(Integer.MAX_VALUE);
        }
        
    }
    
    public void sobel(){
        temp = new int[3][3];
        
        
        for(int i = 1 ; i < image.getWidth()-1 ; i++){
            for(int j = 1 ; j < image.getHeight()-1 ; j++){
               
                temp = assignNewPixel(i, j, 16);
                int newRed = convolutionSobel(temp);
//                
//                temp = assignNewPixel(i, j, 8);
//                int newGreen = convolution(temp);
//                
//                temp = assignNewPixel(i, j, 0);              
//                int newBlue = convolution(temp);
  
//                Color color = new Color(newRed<<16, newRed<<8, newRed);
                
  //              int rgb = (int)color.getRGB();
                imageOutput.setRGB(i, j, (newRed<<16|newRed<<8|newRed));
                
            }
        }
        image = imageOutput;
        revalidate();
        repaint();
    }
    
    
    public void sharpening(){
        temp = new int[3][3];
        
        
        for(int i = 1 ; i < image.getWidth()-1 ; i++){
            for(int j = 1 ; j < image.getHeight()-1 ; j++){
               
                temp = assignNewPixel(i, j, 16);
                int newRed = convolutionSharpening(temp);
                
                temp = assignNewPixel(i, j, 8);
                int newGreen = convolutionSharpening(temp);
                
                temp = assignNewPixel(i, j, 0);              
                int newBlue = convolutionSharpening(temp);
                
                if(newRed < 0) newRed = 0;
                else if(newRed > 255) newRed = 255;
                
                if(newGreen < 0) newGreen = 0;
                else if(newGreen > 255) newGreen = 255;
                
                if(newBlue < 0) newBlue = 0;
                else if(newBlue > 255) newBlue = 255;
                
                Color color = new Color(newRed, newGreen, newBlue);
                
                int rgb = (int)color.getRGB();
                imageOutput.setRGB(i, j, rgb);
                
            }
        }
        image = imageOutput;
        revalidate();
        repaint();
    }   
    
    public int[][] assignNewPixel(int i, int j, int bitshift){
        int pixel = image.getRGB(i-1,j-1);
                    int color = (pixel >> bitshift) & 0xff;
                    temp[0][0] = color;
                    
                    pixel = image.getRGB(i-1,j);
                    color = (pixel >> bitshift) & 0xff;
                    temp[0][1] = color;
                    
                    pixel = image.getRGB(i-1,j+1);
                    color = (pixel >> bitshift) & 0xff;
                    temp[0][2] = color;
                    
                    pixel = image.getRGB(i,j-1);
                    color = (pixel >> bitshift) & 0xff;
                    temp[1][0] = color;
                    
                    pixel = image.getRGB(i,j);
                    color = (pixel >> bitshift) & 0xff;
                    temp[1][1] = color;
                    
                    pixel = image.getRGB(i,j+1);
                    color = (pixel >> bitshift) & 0xff;
                    temp[1][2] = color;
                    
                    pixel = image.getRGB(i+1,j-1);
                    color = (pixel >> bitshift) & 0xff;
                    temp[2][0] = color;
                    
                    pixel = image.getRGB(i+1,j);
                    color = (pixel >> bitshift) & 0xff;
                    temp[2][1] = color;
                    
                    pixel = image.getRGB(i+1,j+1);
                    color = (pixel >> bitshift) & 0xff;
                    temp[2][2] = color;
                    
                    return temp;
                
    }
    
    public int convolutionSobel(int[][] pixelMatrix){
        
        int gy=(pixelMatrix[0][0]*-1)+(pixelMatrix[0][1]*-2)+(pixelMatrix[0][2]*-1)+(pixelMatrix[2][0])+(pixelMatrix[2][1]*2)+(pixelMatrix[2][2]*1);
        int gx=(pixelMatrix[0][0])+(pixelMatrix[0][2]*-1)+(pixelMatrix[1][0]*2)+(pixelMatrix[1][2]*-2)+(pixelMatrix[2][0])+(pixelMatrix[2][2]*-1);
        
        return (int) Math.sqrt(Math.pow(gy,2)+Math.pow(gx,2));
    }
    
    public int convolutionSharpening(int[][] pixelMatrix){
        
        return (pixelMatrix[0][0]*-1) + (pixelMatrix[0][1]*-1) + (pixelMatrix[0][2]*-1) +
                (pixelMatrix[1][0]*-1) + (pixelMatrix[1][1]*9) + (pixelMatrix[1][2]*-1) +
                (pixelMatrix[2][0]*-1) + (pixelMatrix[2][1]*-1) + (pixelMatrix[2][2]*-1);
        
        
    }
    
    @Override
    public Dimension getPreferredSize() {
        return image == null ? new Dimension(400, 300): new Dimension(image.getWidth(), image.getHeight());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    }
}